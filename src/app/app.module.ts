import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListSalesComponent } from './components/list-sales/list-sales.component';
import { HelloComponent } from './components/hello/hello.component';
import { TravelComponent } from './components/travel/travel.component';
import { MathComponent } from './components/math/math.component';

@NgModule({
  declarations: [
    AppComponent,
    ListSalesComponent,
    HelloComponent,
    TravelComponent,
    MathComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
