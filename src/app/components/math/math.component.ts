import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-math',
  templateUrl: './math.component.html',
  styleUrls: ['./math.component.css']
})
export class MathComponent implements OnInit {
   // properties

   num1: number = 8;
   num2: number = 12;


  constructor() { }

  ngOnInit(): void {

    this.sum();
    this.difference();
    this.product();
    this.quotient();
    // this.fizzbuzz();
  }

  // methods

  sum() {
    return this.num1 + this.num2
  }

  difference() {
    return this.num1 - this.num2
  }

  product() {
    return this.num1 * this.num2
  }

  quotient() {
    return this.num1 / this.num2
  }





  // fizz buzz

// fizzbuzz() {
//   for (var i = 1; i <= 100; i++){
//     if(i % 3 === 0 && i % 5 === 0) {
//       return "FizzBuzz"
//     }
//     else if ( i % 3 === 0) {
//       return "Fizz"
//     }
//     else if ( i % 5 === 0) {
//       return "Buzz"
//     }
//     else {
//       return "i"
//     }
//   }
// }

} // end of class


