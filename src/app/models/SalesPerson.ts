export interface SalesPerson{
    firstName: string;
    lastName: string;
    email: string;
    quota: number;
}