import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { SalesPerson } from '../models/SalesPerson';

@Injectable({
  providedIn: 'root'
})
export class SalesService {


  // property
  salesTeam: SalesPerson[] = [];

  constructor() {
    this.salesTeam = [ {
      firstName: "John",
      lastName: "Cena",
      email: "john@email.com",
      quota: 75000
    }, 
    {
      firstName: "Charlotte",
      lastName: "Flair",
      email: "charlotte@email.com",
      quota: 85000
    },
    {
      firstName: "Danile",
      lastName: "Bryan",
      email: "daniel@email.com",
      quota: 80000
    }
  ]
   }

   // method
   getSalesTeam() : Observable<SalesPerson[]> {
     return of(this.salesTeam);
   }

}
